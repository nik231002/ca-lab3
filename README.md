 # Forth. Транслятор и модель

- Кошкарбаев Никита P33121.
- `forth | stack | neum | hw | instr | struct | trap | mem | prob2`

## Язык программирования

``` ebnf
program ::= term

term ::= command
        | var_init
        | string_init
        | push_symbol
        | term term

command ::= "NOP" | "MOD" | "+" | "<" | "NEGATE" | "INVERT" | "DUP" | "OVER" | "ROT" | "SWAP" | "DROP" 
            | "INTERRUPT" | "INTERRUPT_END" 
            | "IF" | "ELSE" | "ENDIF" 
            | "WHILE" | "BEGIN" | "REPEAT"
            | READ | READ# | WR | WR#

push_symbol = [-2^31; 2^31], ["a"; "z"]

string_init = (string_name): string_value

var_init ::= var_name: var_value 
                | &string_name

```

Код выполняется последовательно. Операции:

- `OVER` -- ... `a b` >> ... `a b a` 
- `DUP` -- ... `a` >> ... `a a`
- `<` -- ... `a b` >> ... `a<b`
- `MOD` -- ... `a b` >> ... `a%b`
- `NEGATE` -- ... `a` >> ... `-a`
- `INVERT` -- ... `0` >> ... `-1`
- `+` -- ... `a b` >> ... `a+b`
- `DROP` -- ... `a b` >> ... `a`
- `ROT` -- ... `a b c` >> ... `b c a`
- `SWAP` -- ... `a b` >> ... `b a`
- `{num}` -- ... `a b` >> ... `a b {num}`
- `{char}` -- ... `a b` >> ... `a b {char}`
- `INTERRUPT` -- начало блока кода, для обработки прерывания
- `INTERRUPT_END` -- конец блока кода, для обработки прерывания
- `IF` -- возьмёт со стека значение, и если оно True, то перейдёт далее. Если False, то прыгнет на ELSE
- `ELSE` -- сработает в случае, если при IF на стеке лежало False
- `ENDIF` -- закрывает блок кода, принадлежащий IF
- `BEGIN` -- является меткой, для возвращения назад, когда мы в цикле
- `WHILE` -- возьмёт со стека значение, и если оно True, то перейдёт далее. Если False, то прыгнет на команду, на +1 дальше, чем REPEAT
- `REPEAT` -- возвращает на BEGIN в случае если во время WHILE было True 
- `NOP` -- заглушка. не выполняет никаких операций
- `READ` (чтение с прямой адресацией)-- берёт со стека значение `x` и кладёт на стек значение memory[x]
- `READ#` (чтение с косвенной адресацией) -- берёт со стека значение `x`, кладёт на стек значение memory[memory[x]], и увеличивает значение memory[x] на +1
- `WR` (запись с прямой адресацией) -- берёт со стека значение `x`, и значение `y` и записывает в память memory[x] = y
- `WR#` (запись с косвенной адресацией) -- берёт со стека значение `x`, и значение `y` и записывает в память memory[memory[x]] = y, и увеличивает значение memory[x] на +1

## Организация памяти
Модель памяти процессора:

1. Память команд. Машинное слово -- не определено. Реализуется списком словарей, описывающих инструкции (одно слово -- одна ячейка).
2. Память данных. Машинное слово -- 32 бита, знаковое. Линейное адресное пространство. Реализуется списком чисел.
3. Стек. Заменяет регистры. Машинное слово -- 32 бита, знаковое. Линейное адресное пространство. Реализуется списком чисел.

Строки, объявленные пользователем распеделяются по памяти один символ на ячейку.

```text
     Instruction memory
+-----------------------------+
| 00  : jmp N                 |
| 01  : interrupt handler     |
|    ...                      | 
| n   : program start         |
|    ...                      |
+-----------------------------+

Data memory
+-----------------------------+
| 00  : input                 |
| 01  : output                |
| 03  : data                  |
|    ...                      |
| 127 : data                  |
| 127 : program               |
| 128 : program               |
|    ...                      |
+-----------------------------+
```

## Система команд

Особенности процессора:

- Машинное слово -- 32 бит, знаковое.
- `Память`:
    - адресуется через регистр `data_address`;
    - может быть записана:
        - с порта ввода;
        - с макушки стека;
    - может быть прочитана:
        - в буффер;
- `Стек`:
    - адресуется через регистр `data_head`;
    - Верхушка стека (верхние 3 элемента):
        - может быть записана:
            - из буфера;
        - может быть прочитана:
            - на вход АЛУ
            - на вывод
        - используется как флаг `is_true` 1 - на верхушке `True`, иначе `False`
- Ввод-вывод -- memory-mapped, по прерыванию.
- `program_counter` -- счётчик команд:
    - инкрементируется после каждой инструкции или перезаписывается инструкцией перехода.
- `ready` - флаг готовности:
    - 0 - если процессор должен обработать входящее прерывание, 1 - процессор не может обработать входящее прерывание/входящих прерываний нет
- `interrupt` - обрабатывается ли на данный момент прерывание.

### Набор инструкции

| Syntax         | Mnemonic        | Кол-во тактов | Comment                           |
|:---------------|:----------------|---------------|:----------------------------------|
| `OVER`         | OVER            | 1             | см. язык                          |
| `DUP`          | DUP             | 1             | см. язык                          |
| `<`            | LT              | 2             | см. язык                          |
| `MOD`          | MOD             | 2             | см. язык                          |
| `NEGATE`       | NEG             | 2             | см. язык                          |
| `INVERT`       | INV             | 2             | см. язык                          |
| `+`            | PLUS            | 2             | см. язык                          |
| `DROP`         | DROP            | 1             | см. язык                          |
| `ROT`          | ROT             | 1             | см. язык                          |
| `SWAP`         | SWAP            | 1             | см. язык                          |
| `{sym}`        | PUSH `{sym}`    | 1             | см. язык                          |
| `INTERRUPT`    |                 | -             | см. язык                          |
| `INTERRUPT_END`| INTEND          | 1             | см. язык                          |
| `IF`           | JNT {ELSE + 1}  | 1             | см. язык                          |
| `ELSE`         | JMP {ENDIF}     | 1             | см. язык                          |
| `ENDIF`        | NOP             | 1             | см. язык                          |
| `BEGIN`        | BEGIN           | 1             | см. язык                          |
| `WHILE`        | JNT {REPEAT + 1}| 1             | см. язык                          |
| `REPEAT`       | JMP {BEGIN + 1} | 1             | см. язык                          |
| `NOP`          | NOP             | 1             | см. язык                          |
| `WR`           | WR_DIR          | 2             | см. язык                          |
| `WR#`          | WR_NDR          | 6             | см. язык                          |
| `READ`         | READ_DIR        | 2             | см. язык                          |
| `READ#`        | READ_NDR        | 7             | см. язык                          |


### Кодирование инструкций

- Машинный код сериализуется в список JSON.
- Один элемент списка, одна инструкция.
- Индекс списка -- адрес инструкции. Используется для команд перехода.

Пример:

```json
[
   {
        "opcode": "PUSH",
        "arg": "0",
        "term": [
            1,
            "PUSH",
            "0"
        ]
    }
]
```

где:

- `opcode` -- строка с кодом операции;
- `arg` -- аргумент (может отсутствовать);
- `term` -- информация о связанном месте в исходном коде (если есть).

Типы данные в модуле [isa](./isa.py), где:

- `Opcode` -- перечисление кодов операций;
- `Term` -- структура для описания значимого фрагмента кода исходной программы.

## Транслятор

Интерфейс командной строки: `translator.py <input_file> <target_file> <data_section_file>"`

Реализовано в модуле: [translator](./translator.py)

Этапы трансляции (функция `translate`):
1. Трансформирование текста в последовательность значимых термов.
    - Переменные:
        - Транслируются в соответствующие значения на этапе трансляции.
        - Задаётся либо числовым значением, либо указателем на начало строки (используя &string_name)
2. Проверка корректности программы (одинаковое количество IF, ELSE, ENDIF и BEGIN, WHILE, REPEAT).
3. Генерация машинного кода.

Правила генерации машинного кода:

- одно слово языка -- одна инструкция;
- для команд, однозначно соответствующих инструкциям -- прямое отображение;
- для циклов с соблюдением парности (многоточие -- произвольный код):

    | Номер команды/инструкции | Программа | Машинный код |
    |:-------------------------|:----------|:-------------|
    | n                        | `BEGIN`   | `BEGIN`      |
    | ...                      | ...       | ...          |
    | n+3                      | `WHILE`   | `JNT (k+1)`  |
    | ...                      | ...       | ...          |
    | k                        | `REPEAT`  |  `JMP (n+1)` |
    | k+1                      | ...       | ...          |
- для условных операторов (многоточие -- произвольный код):

    | Номер команды/инструкции | Программа | Машинный код |
    |:-------------------------|:----------|:-------------|
    | n                        | `IF`      | `JNT n+4`    |
    | ...                      | ...       | ...          |
    | n+3                      | `ELSE`    | `JMP (k+1)`  |
    | ...                      | ...       | ...          |
    | k                        | `ENDIF`   |  `NOP`       |
    | k+1                      | ...       | ...          |
- для обработчиков прерываний:
    | Номер команды/инструкции | Программа         | Машинный код |
    |:-------------------------|:------------------|:-------------|
    | 0                        | `JMP`             | `JMP n`      |
    | 1                        | `INTERRUPT PROG`  | ...          |
    | ...                      | ...               | ...          |
    | n-1                      | `INTERRUPT END`   | `INTEND`     |
    | n                        | `MAIN PROG`       |  `...`       |
    | ...                      | ...               | ...          |

## Модель процессора

Реализовано в модуле: [machine](./machine.py).

![machine](img/machine.png)

Сигналы (обрабатываются за один такт, реализованы в виде методов класса Memory):

- `wr` -- записать в data memory
- `oe` -- прочитать из data memory 
- `latch_data_address` -- защёлкнуть значение в `data_address`

### DataPath

![data path](img/datapath.png)

Сигналы (обрабатываются за один такт, реализованы в виде методов класса):

- `push` -- записать на макушку стека значение из АЛУ, input или argument
- `oe_stack` -- прочитать из макушки стека и отправить в output
- `latch_data_head` -- защёлкнуть значение в `data_head`

Флаги:

- `is_true` -- лежит ли на стеке true.

### АЛУ

![alu](img/alu.png)

Сигналы (обрабатываются за один такт, реализованы в виде методов класса):

- `operation_sel` -- произвести операцию

### ControlUnit

![control unit](img/control_unit.png)

Реализован в классе `control_unit`.

- Hardwired (реализовано полностью на python).
- Моделирование на уровне инструкций.
- Трансляция инструкции в последовательность (0-3 тактов) сигналов: `decode_and_execute`.

Сигнал:

- `latch_program_couter` -- сигнал для обновления счётчика команд в control_unit.
- `on/off` -- переключить тумблер ready (либо 0, либо 1).
- `latch_context` -- сигнал для защёлкивания значения context.
- `y/n` -- переключить значение interrupt (либо 0, либо 1).

Особенности работы модели:

- Для журнала состояний процессора используется стандартный модуль logging.
- Количество инструкций для моделирования ограничено hardcoded константой.
- Остановка моделирования осуществляется при помощи исключений:
    - `EOFError` -- если нет данных для чтения из порта ввода-вывода;
    - `StopIteration` -- если выполнена инструкция `halt`.
- Управление симуляцией реализовано в функции `simulate`.

## Апробация

В качестве тестов для `machine` использовано 5 тестов:

1. [hello world](examples/hello_world_code).
2. [cat](examples/cat_code) -- программа `cat`, повторяем ввод на выводе.
3. [prob2](examples/fib_forth_code) -- программа, решающая 2 проблему Эйлера.
4. [if](examples/if_code) -- программа, проверяющая корректность работы IF.
5. [while](examples/while_code) -- программа, проверяющая корректность работы WHILE.

В качестве тестов для `translator` использовано 2 теста:
1.  [cat](examples/correct_cat_code) -- программа `cat`, повторяем ввод на выводе.
2. [prob2](examples/correct_fib_forth_code) -- программа, решающая 2 проблему Эйлера.

Интеграционные тесты реализованы тут: [machine_integration_test](./machine_integration_test.py) и [translator_integration_test](./translator_integration_test.py)

CI:

``` yaml
lab3-example:
  stage: test
  image:
    name: python-tools
    entrypoint: [""]
  script:
    - python3-coverage run -m pytest --verbose
    - find . -type f -name "*.py" | xargs -t python3-coverage report
    - find . -type f -name "*.py" | xargs -t pep8 --ignore=E501
    - find . -type f -name "*.py" | xargs -t pylint
```

где:

- `python3-coverage` -- формирование отчёта об уровне покрытия исходного кода.
- `pytest` -- утилита для запуска тестов.
- `pep8` -- утилита для проверки форматирования кода. `E501` (длина строк) отключено, но не следует этим злоупотреблять.
- `pylint` -- утилита для проверки качества кода. Некоторые правила отключены в отдельных модулях с целью упрощения кода.
- Docker image `python-tools` включает в себя все перечисленные утилиты. Его конфигурация: [Dockerfile](./Dockerfile).

Пример использования и журнал работы процессора на примере `cat`:

``` console
> cat examples/cat_code
INTERRUPT
#in
READ
#out
WR
INTERRUPT_END
BEGIN
-1
WHILE
REPEAT
> cat examples/input_hello
[
    {"tick": 1, "char": "h"},
    {"tick": 2, "char": "e"},
    {"tick": 6, "char": "l"},
    {"tick": 8, "char": "l"},
    {"tick": 45, "char": "o"}
]
> ./translator.py examples/cat.code target.out examples/data_section
source LoC: 10 code instr: 11
> cat target.out 
[
    {
        "opcode": "JMP",
        "arg": 6,
        "term": [
            0,
            "JMP",
            6
        ]
    },
    {
        "opcode": "PUSH",
        "arg": 0,
        "term": [
            2,
            "PUSH",
            0
        ]
    },
    {
        "opcode": "READ_DIR",
        "term": [
            3,
            "READ",
            null
        ]
    },
    {
        "opcode": "PUSH",
        "arg": 1,
        "term": [
            4,
            "PUSH",
            1
        ]
    },
    {
        "opcode": "WRITE_DIR",
        "term": [
            5,
            "WR",
            null
        ]
    },
    {
        "opcode": "INTEND",
        "term": [
            6,
            "INTERRUPT_END",
            null
        ]
    },
    {
        "opcode": "BEGIN",
        "term": [
            7,
            "BEGIN",
            null
        ]
    },
    {
        "opcode": "PUSH",
        "arg": "-1",
        "term": [
            8,
            "PUSH",
            "-1"
        ]
    },
    {
        "opcode": "JNT",
        "arg": 10,
        "term": [
            9,
            "WHILE",
            null
        ]
    },
    {
        "opcode": "JMP",
        "arg": 7,
        "term": [
            10,
            "REPEAT",
            null
        ]
    },
    {
        "opcode": "HALT",
        "term": [
            11,
            "HALT",
            null
        ]
    }
]
> ./control_unit.py target.out examples/data_section examples/input_hello
DEBUG:root:{TICK: 0, PC: 0, HEAD: 0, TOS: 0, 0, 0}  JMP 6 ('6' @ 0:JMP)
DEBUG:root:Interrupting TICK: {1} 
DEBUG:root:104
DEBUG:root:{TICK: 1, PC: 1, HEAD: 0, TOS: 0, 0, 0}  PUSH 0 ('0' @ 2:PUSH)
DEBUG:root:{TICK: 2, PC: 2, HEAD: 1, TOS: 0, 0, 0}  READ_DIR  ('None' @ 3:READ)
DEBUG:root:{TICK: 4, PC: 3, HEAD: 1, TOS: 0, 0, 104}  PUSH 1 ('1' @ 4:PUSH)
DEBUG:root:{TICK: 5, PC: 4, HEAD: 2, TOS: 0, 104, 1}  WRITE_DIR  ('None' @ 5:WR)
INFO:root:output:  << h
DEBUG:root:{TICK: 7, PC: 5, HEAD: 0, TOS: 0, 0, 0}  INTEND  ('None' @ 6:INTERRUPT_END)
DEBUG:root:interrupting ends
DEBUG:root:Interrupting TICK: {8} 
DEBUG:root:101
DEBUG:root:{TICK: 8, PC: 1, HEAD: 0, TOS: 0, 0, 0}  PUSH 0 ('0' @ 2:PUSH)
DEBUG:root:{TICK: 9, PC: 2, HEAD: 1, TOS: 0, 0, 0}  READ_DIR  ('None' @ 3:READ)
DEBUG:root:{TICK: 11, PC: 3, HEAD: 1, TOS: 0, 0, 101}  PUSH 1 ('1' @ 4:PUSH)
DEBUG:root:{TICK: 12, PC: 4, HEAD: 2, TOS: 0, 101, 1}  WRITE_DIR  ('None' @ 5:WR)
INFO:root:output: h << e
DEBUG:root:{TICK: 14, PC: 5, HEAD: 0, TOS: 0, 0, 0}  INTEND  ('None' @ 6:INTERRUPT_END)
DEBUG:root:interrupting ends
DEBUG:root:Interrupting TICK: {15}
DEBUG:root:108
DEBUG:root:{TICK: 15, PC: 1, HEAD: 0, TOS: 0, 0, 0}  PUSH 0 ('0' @ 2:PUSH)
DEBUG:root:{TICK: 16, PC: 2, HEAD: 1, TOS: 0, 0, 0}  READ_DIR  ('None' @ 3:READ)
DEBUG:root:{TICK: 18, PC: 3, HEAD: 1, TOS: 0, 0, 108}  PUSH 1 ('1' @ 4:PUSH)
DEBUG:root:{TICK: 19, PC: 4, HEAD: 2, TOS: 0, 108, 1}  WRITE_DIR  ('None' @ 5:WR)
INFO:root:output: h,e << l
DEBUG:root:{TICK: 21, PC: 5, HEAD: 0, TOS: 0, 0, 0}  INTEND  ('None' @ 6:INTERRUPT_END)
DEBUG:root:interrupting ends
DEBUG:root:Interrupting TICK: {22}
DEBUG:root:108
DEBUG:root:{TICK: 22, PC: 1, HEAD: 0, TOS: 0, 0, 0}  PUSH 0 ('0' @ 2:PUSH)
DEBUG:root:{TICK: 23, PC: 2, HEAD: 1, TOS: 0, 0, 0}  READ_DIR  ('None' @ 3:READ)
DEBUG:root:{TICK: 25, PC: 3, HEAD: 1, TOS: 0, 0, 108}  PUSH 1 ('1' @ 4:PUSH)
DEBUG:root:{TICK: 26, PC: 4, HEAD: 2, TOS: 0, 108, 1}  WRITE_DIR  ('None' @ 5:WR)
INFO:root:output: h,e,l << l
DEBUG:root:{TICK: 28, PC: 5, HEAD: 0, TOS: 0, 0, 0}  INTEND  ('None' @ 6:INTERRUPT_END)
DEBUG:root:interrupting ends
DEBUG:root:{TICK: 29, PC: 6, HEAD: 0, TOS: 0, 0, 0}  BEGIN  ('None' @ 7:BEGIN)
DEBUG:root:{TICK: 30, PC: 7, HEAD: 0, TOS: 0, 0, 0}  PUSH -1 ('-1' @ 8:PUSH)
DEBUG:root:{TICK: 31, PC: 8, HEAD: 1, TOS: 0, 0, -1}  JNT 10 ('None' @ 9:WHILE)
DEBUG:root:{TICK: 32, PC: 9, HEAD: 0, TOS: 0, 0, 0}  JMP 7 ('None' @ 10:REPEAT)
DEBUG:root:{TICK: 33, PC: 7, HEAD: 0, TOS: 0, 0, 0}  PUSH -1 ('-1' @ 8:PUSH)
DEBUG:root:{TICK: 34, PC: 8, HEAD: 1, TOS: 0, 0, -1}  JNT 10 ('None' @ 9:WHILE)
DEBUG:root:{TICK: 35, PC: 9, HEAD: 0, TOS: 0, 0, 0}  JMP 7 ('None' @ 10:REPEAT)
DEBUG:root:{TICK: 36, PC: 7, HEAD: 0, TOS: 0, 0, 0}  PUSH -1 ('-1' @ 8:PUSH)
DEBUG:root:{TICK: 37, PC: 8, HEAD: 1, TOS: 0, 0, -1}  JNT 10 ('None' @ 9:WHILE)
DEBUG:root:{TICK: 38, PC: 9, HEAD: 0, TOS: 0, 0, 0}  JMP 7 ('None' @ 10:REPEAT)
DEBUG:root:{TICK: 39, PC: 7, HEAD: 0, TOS: 0, 0, 0}  PUSH -1 ('-1' @ 8:PUSH)
DEBUG:root:{TICK: 40, PC: 8, HEAD: 1, TOS: 0, 0, -1}  JNT 10 ('None' @ 9:WHILE)
DEBUG:root:{TICK: 41, PC: 9, HEAD: 0, TOS: 0, 0, 0}  JMP 7 ('None' @ 10:REPEAT)
DEBUG:root:{TICK: 42, PC: 7, HEAD: 0, TOS: 0, 0, 0}  PUSH -1 ('-1' @ 8:PUSH)
DEBUG:root:{TICK: 43, PC: 8, HEAD: 1, TOS: 0, 0, -1}  JNT 10 ('None' @ 9:WHILE)
DEBUG:root:{TICK: 44, PC: 9, HEAD: 0, TOS: 0, 0, 0}  JMP 7 ('None' @ 10:REPEAT)
DEBUG:root:Interrupting TICK: {45}
DEBUG:root:111
DEBUG:root:{TICK: 45, PC: 1, HEAD: 0, TOS: 0, 0, 0}  PUSH 0 ('0' @ 2:PUSH)
DEBUG:root:{TICK: 46, PC: 2, HEAD: 1, TOS: 0, 0, 0}  READ_DIR  ('None' @ 3:READ)
DEBUG:root:{TICK: 48, PC: 3, HEAD: 1, TOS: 0, 0, 111}  PUSH 1 ('1' @ 4:PUSH)
DEBUG:root:{TICK: 49, PC: 4, HEAD: 2, TOS: 0, 111, 1}  WRITE_DIR  ('None' @ 5:WR)
INFO:root:output: h,e,l,l << o
DEBUG:root:{TICK: 51, PC: 5, HEAD: 0, TOS: 0, 0, 0}  INTEND  ('None' @ 6:INTERRUPT_END)
DEBUG:root:interrupting ends
DEBUG:root:{TICK: 52, PC: 7, HEAD: 0, TOS: 0, 0, 0}  PUSH -1 ('-1' @ 8:PUSH)
DEBUG:root:{TICK: 53, PC: 8, HEAD: 1, TOS: 0, 0, -1}  JNT 10 ('None' @ 9:WHILE)
DEBUG:root:{TICK: 54, PC: 9, HEAD: 0, TOS: 0, 0, 0}  JMP 7 ('None' @ 10:REPEAT)
DEBUG:root:{TICK: 55, PC: 7, HEAD: 0, TOS: 0, 0, 0}  PUSH -1 ('-1' @ 8:PUSH)
DEBUG:root:{TICK: 56, PC: 8, HEAD: 1, TOS: 0, 0, -1}  JNT 10 ('None' @ 9:WHILE)
DEBUG:root:{TICK: 57, PC: 9, HEAD: 0, TOS: 0, 0, 0}  JMP 7 ('None' @ 10:REPEAT)
DEBUG:root:{TICK: 58, PC: 7, HEAD: 0, TOS: 0, 0, 0}  PUSH -1 ('-1' @ 8:PUSH)
DEBUG:root:{TICK: 59, PC: 8, HEAD: 1, TOS: 0, 0, -1}  JNT 10 ('None' @ 9:WHILE)
DEBUG:root:{TICK: 60, PC: 9, HEAD: 0, TOS: 0, 0, 0}  JMP 7 ('None' @ 10:REPEAT)
DEBUG:root:{TICK: 61, PC: 7, HEAD: 0, TOS: 0, 0, 0}  PUSH -1 ('-1' @ 8:PUSH)
DEBUG:root:{TICK: 62, PC: 8, HEAD: 1, TOS: 0, 0, -1}  JNT 10 ('None' @ 9:WHILE)
DEBUG:root:{TICK: 63, PC: 9, HEAD: 0, TOS: 0, 0, 0}  JMP 7 ('None' @ 10:REPEAT)
DEBUG:root:{TICK: 64, PC: 7, HEAD: 0, TOS: 0, 0, 0}  PUSH -1 ('-1' @ 8:PUSH)
DEBUG:root:{TICK: 65, PC: 8, HEAD: 1, TOS: 0, 0, -1}  JNT 10 ('None' @ 9:WHILE)
DEBUG:root:{TICK: 66, PC: 9, HEAD: 0, TOS: 0, 0, 0}  JMP 7 ('None' @ 10:REPEAT)
DEBUG:root:{TICK: 67, PC: 7, HEAD: 0, TOS: 0, 0, 0}  PUSH -1 ('-1' @ 8:PUSH)
DEBUG:root:{TICK: 68, PC: 8, HEAD: 1, TOS: 0, 0, -1}  JNT 10 ('None' @ 9:WHILE)
DEBUG:root:{TICK: 69, PC: 9, HEAD: 0, TOS: 0, 0, 0}  JMP 7 ('None' @ 10:REPEAT)
DEBUG:root:{TICK: 70, PC: 7, HEAD: 0, TOS: 0, 0, 0}  PUSH -1 ('-1' @ 8:PUSH)
DEBUG:root:{TICK: 71, PC: 8, HEAD: 1, TOS: 0, 0, -1}  JNT 10 ('None' @ 9:WHILE)
DEBUG:root:{TICK: 72, PC: 9, HEAD: 0, TOS: 0, 0, 0}  JMP 7 ('None' @ 10:REPEAT)
DEBUG:root:{TICK: 73, PC: 7, HEAD: 0, TOS: 0, 0, 0}  PUSH -1 ('-1' @ 8:PUSH)
DEBUG:root:{TICK: 74, PC: 8, HEAD: 1, TOS: 0, 0, -1}  JNT 10 ('None' @ 9:WHILE)
DEBUG:root:{TICK: 75, PC: 9, HEAD: 0, TOS: 0, 0, 0}  JMP 7 ('None' @ 10:REPEAT)
DEBUG:root:{TICK: 76, PC: 7, HEAD: 0, TOS: 0, 0, 0}  PUSH -1 ('-1' @ 8:PUSH)
DEBUG:root:{TICK: 77, PC: 8, HEAD: 1, TOS: 0, 0, -1}  JNT 10 ('None' @ 9:WHILE)
DEBUG:root:{TICK: 78, PC: 9, HEAD: 0, TOS: 0, 0, 0}  JMP 7 ('None' @ 10:REPEAT)
DEBUG:root:{TICK: 79, PC: 7, HEAD: 0, TOS: 0, 0, 0}  PUSH -1 ('-1' @ 8:PUSH)
DEBUG:root:{TICK: 80, PC: 8, HEAD: 1, TOS: 0, 0, -1}  JNT 10 ('None' @ 9:WHILE)
DEBUG:root:{TICK: 81, PC: 9, HEAD: 0, TOS: 0, 0, 0}  JMP 7 ('None' @ 10:REPEAT)
DEBUG:root:{TICK: 82, PC: 7, HEAD: 0, TOS: 0, 0, 0}  PUSH -1 ('-1' @ 8:PUSH)
DEBUG:root:{TICK: 83, PC: 8, HEAD: 1, TOS: 0, 0, -1}  JNT 10 ('None' @ 9:WHILE)
DEBUG:root:{TICK: 84, PC: 9, HEAD: 0, TOS: 0, 0, 0}  JMP 7 ('None' @ 10:REPEAT)
DEBUG:root:{TICK: 85, PC: 7, HEAD: 0, TOS: 0, 0, 0}  PUSH -1 ('-1' @ 8:PUSH)
DEBUG:root:{TICK: 86, PC: 8, HEAD: 1, TOS: 0, 0, -1}  JNT 10 ('None' @ 9:WHILE)
DEBUG:root:{TICK: 87, PC: 9, HEAD: 0, TOS: 0, 0, 0}  JMP 7 ('None' @ 10:REPEAT)
DEBUG:root:{TICK: 88, PC: 7, HEAD: 0, TOS: 0, 0, 0}  PUSH -1 ('-1' @ 8:PUSH)
DEBUG:root:{TICK: 89, PC: 8, HEAD: 1, TOS: 0, 0, -1}  JNT 10 ('None' @ 9:WHILE)
DEBUG:root:{TICK: 90, PC: 9, HEAD: 0, TOS: 0, 0, 0}  JMP 7 ('None' @ 10:REPEAT)
DEBUG:root:{TICK: 91, PC: 7, HEAD: 0, TOS: 0, 0, 0}  PUSH -1 ('-1' @ 8:PUSH)
DEBUG:root:{TICK: 92, PC: 8, HEAD: 1, TOS: 0, 0, -1}  JNT 10 ('None' @ 9:WHILE)
DEBUG:root:{TICK: 93, PC: 9, HEAD: 0, TOS: 0, 0, 0}  JMP 7 ('None' @ 10:REPEAT)
DEBUG:root:{TICK: 94, PC: 7, HEAD: 0, TOS: 0, 0, 0}  PUSH -1 ('-1' @ 8:PUSH)
DEBUG:root:{TICK: 95, PC: 8, HEAD: 1, TOS: 0, 0, -1}  JNT 10 ('None' @ 9:WHILE)
DEBUG:root:{TICK: 96, PC: 9, HEAD: 0, TOS: 0, 0, 0}  JMP 7 ('None' @ 10:REPEAT)
DEBUG:root:{TICK: 97, PC: 7, HEAD: 0, TOS: 0, 0, 0}  PUSH -1 ('-1' @ 8:PUSH)
DEBUG:root:{TICK: 98, PC: 8, HEAD: 1, TOS: 0, 0, -1}  JNT 10 ('None' @ 9:WHILE)
DEBUG:root:{TICK: 99, PC: 9, HEAD: 0, TOS: 0, 0, 0}  JMP 7 ('None' @ 10:REPEAT)
DEBUG:root:{TICK: 100, PC: 7, HEAD: 0, TOS: 0, 0, 0}  PUSH -1 ('-1' @ 8:PUSH)
DEBUG:root:{TICK: 101, PC: 8, HEAD: 1, TOS: 0, 0, -1}  JNT 10 ('None' @ 9:WHILE)
DEBUG:root:{TICK: 102, PC: 9, HEAD: 0, TOS: 0, 0, 0}  JMP 7 ('None' @ 10:REPEAT)
DEBUG:root:{TICK: 103, PC: 7, HEAD: 0, TOS: 0, 0, 0}  PUSH -1 ('-1' @ 8:PUSH)
DEBUG:root:{TICK: 104, PC: 8, HEAD: 1, TOS: 0, 0, -1}  JNT 10 ('None' @ 9:WHILE)
DEBUG:root:{TICK: 105, PC: 9, HEAD: 0, TOS: 0, 0, 0}  JMP 7 ('None' @ 10:REPEAT)
DEBUG:root:{TICK: 106, PC: 7, HEAD: 0, TOS: 0, 0, 0}  PUSH -1 ('-1' @ 8:PUSH)
DEBUG:root:{TICK: 107, PC: 8, HEAD: 1, TOS: 0, 0, -1}  JNT 10 ('None' @ 9:WHILE)
DEBUG:root:{TICK: 108, PC: 9, HEAD: 0, TOS: 0, 0, 0}  JMP 7 ('None' @ 10:REPEAT)
DEBUG:root:{TICK: 109, PC: 7, HEAD: 0, TOS: 0, 0, 0}  PUSH -1 ('-1' @ 8:PUSH)
DEBUG:root:{TICK: 110, PC: 8, HEAD: 1, TOS: 0, 0, -1}  JNT 10 ('None' @ 9:WHILE)
DEBUG:root:{TICK: 111, PC: 9, HEAD: 0, TOS: 0, 0, 0}  JMP 7 ('None' @ 10:REPEAT)
DEBUG:root:{TICK: 112, PC: 7, HEAD: 0, TOS: 0, 0, 0}  PUSH -1 ('-1' @ 8:PUSH)
DEBUG:root:{TICK: 113, PC: 8, HEAD: 1, TOS: 0, 0, -1}  JNT 10 ('None' @ 9:WHILE)
DEBUG:root:{TICK: 114, PC: 9, HEAD: 0, TOS: 0, 0, 0}  JMP 7 ('None' @ 10:REPEAT)
DEBUG:root:{TICK: 115, PC: 7, HEAD: 0, TOS: 0, 0, 0}  PUSH -1 ('-1' @ 8:PUSH)
DEBUG:root:{TICK: 116, PC: 8, HEAD: 1, TOS: 0, 0, -1}  JNT 10 ('None' @ 9:WHILE)
DEBUG:root:{TICK: 117, PC: 9, HEAD: 0, TOS: 0, 0, 0}  JMP 7 ('None' @ 10:REPEAT)
DEBUG:root:{TICK: 118, PC: 7, HEAD: 0, TOS: 0, 0, 0}  PUSH -1 ('-1' @ 8:PUSH)
DEBUG:root:{TICK: 119, PC: 8, HEAD: 1, TOS: 0, 0, -1}  JNT 10 ('None' @ 9:WHILE)
DEBUG:root:{TICK: 120, PC: 9, HEAD: 0, TOS: 0, 0, 0}  JMP 7 ('None' @ 10:REPEAT)
DEBUG:root:{TICK: 121, PC: 7, HEAD: 0, TOS: 0, 0, 0}  PUSH -1 ('-1' @ 8:PUSH)
DEBUG:root:{TICK: 122, PC: 8, HEAD: 1, TOS: 0, 0, -1}  JNT 10 ('None' @ 9:WHILE)
DEBUG:root:{TICK: 123, PC: 9, HEAD: 0, TOS: 0, 0, 0}  JMP 7 ('None' @ 10:REPEAT)
DEBUG:root:{TICK: 124, PC: 7, HEAD: 0, TOS: 0, 0, 0}  PUSH -1 ('-1' @ 8:PUSH)
DEBUG:root:{TICK: 125, PC: 8, HEAD: 1, TOS: 0, 0, -1}  JNT 10 ('None' @ 9:WHILE)
DEBUG:root:{TICK: 126, PC: 9, HEAD: 0, TOS: 0, 0, 0}  JMP 7 ('None' @ 10:REPEAT)
DEBUG:root:{TICK: 127, PC: 7, HEAD: 0, TOS: 0, 0, 0}  PUSH -1 ('-1' @ 8:PUSH)
DEBUG:root:{TICK: 128, PC: 8, HEAD: 1, TOS: 0, 0, -1}  JNT 10 ('None' @ 9:WHILE)
DEBUG:root:{TICK: 129, PC: 9, HEAD: 0, TOS: 0, 0, 0}  JMP 7 ('None' @ 10:REPEAT)
DEBUG:root:{TICK: 130, PC: 7, HEAD: 0, TOS: 0, 0, 0}  PUSH -1 ('-1' @ 8:PUSH)
DEBUG:root:{TICK: 131, PC: 8, HEAD: 1, TOS: 0, 0, -1}  JNT 10 ('None' @ 9:WHILE)
DEBUG:root:{TICK: 132, PC: 9, HEAD: 0, TOS: 0, 0, 0}  JMP 7 ('None' @ 10:REPEAT)
DEBUG:root:{TICK: 133, PC: 7, HEAD: 0, TOS: 0, 0, 0}  PUSH -1 ('-1' @ 8:PUSH)
DEBUG:root:{TICK: 134, PC: 8, HEAD: 1, TOS: 0, 0, -1}  JNT 10 ('None' @ 9:WHILE)
DEBUG:root:{TICK: 135, PC: 9, HEAD: 0, TOS: 0, 0, 0}  JMP 7 ('None' @ 10:REPEAT)
DEBUG:root:{TICK: 136, PC: 7, HEAD: 0, TOS: 0, 0, 0}  PUSH -1 ('-1' @ 8:PUSH)
DEBUG:root:{TICK: 137, PC: 8, HEAD: 1, TOS: 0, 0, -1}  JNT 10 ('None' @ 9:WHILE)
DEBUG:root:{TICK: 138, PC: 9, HEAD: 0, TOS: 0, 0, 0}  JMP 7 ('None' @ 10:REPEAT)
DEBUG:root:{TICK: 139, PC: 7, HEAD: 0, TOS: 0, 0, 0}  PUSH -1 ('-1' @ 8:PUSH)
DEBUG:root:{TICK: 140, PC: 8, HEAD: 1, TOS: 0, 0, -1}  JNT 10 ('None' @ 9:WHILE)
DEBUG:root:{TICK: 141, PC: 9, HEAD: 0, TOS: 0, 0, 0}  JMP 7 ('None' @ 10:REPEAT)
DEBUG:root:{TICK: 142, PC: 7, HEAD: 0, TOS: 0, 0, 0}  PUSH -1 ('-1' @ 8:PUSH)
DEBUG:root:{TICK: 143, PC: 8, HEAD: 1, TOS: 0, 0, -1}  JNT 10 ('None' @ 9:WHILE)
DEBUG:root:{TICK: 144, PC: 9, HEAD: 0, TOS: 0, 0, 0}  JMP 7 ('None' @ 10:REPEAT)
DEBUG:root:{TICK: 145, PC: 7, HEAD: 0, TOS: 0, 0, 0}  PUSH -1 ('-1' @ 8:PUSH)
DEBUG:root:{TICK: 146, PC: 8, HEAD: 1, TOS: 0, 0, -1}  JNT 10 ('None' @ 9:WHILE)
DEBUG:root:{TICK: 147, PC: 9, HEAD: 0, TOS: 0, 0, 0}  JMP 7 ('None' @ 10:REPEAT)
DEBUG:root:{TICK: 148, PC: 7, HEAD: 0, TOS: 0, 0, 0}  PUSH -1 ('-1' @ 8:PUSH)
DEBUG:root:{TICK: 149, PC: 8, HEAD: 1, TOS: 0, 0, -1}  JNT 10 ('None' @ 9:WHILE)
DEBUG:root:{TICK: 150, PC: 9, HEAD: 0, TOS: 0, 0, 0}  JMP 7 ('None' @ 10:REPEAT)
DEBUG:root:{TICK: 151, PC: 7, HEAD: 0, TOS: 0, 0, 0}  PUSH -1 ('-1' @ 8:PUSH)
DEBUG:root:{TICK: 152, PC: 8, HEAD: 1, TOS: 0, 0, -1}  JNT 10 ('None' @ 9:WHILE)
DEBUG:root:{TICK: 153, PC: 9, HEAD: 0, TOS: 0, 0, 0}  JMP 7 ('None' @ 10:REPEAT)
DEBUG:root:{TICK: 154, PC: 7, HEAD: 0, TOS: 0, 0, 0}  PUSH -1 ('-1' @ 8:PUSH)
DEBUG:root:{TICK: 155, PC: 8, HEAD: 1, TOS: 0, 0, -1}  JNT 10 ('None' @ 9:WHILE)
DEBUG:root:{TICK: 156, PC: 9, HEAD: 0, TOS: 0, 0, 0}  JMP 7 ('None' @ 10:REPEAT)
DEBUG:root:{TICK: 157, PC: 7, HEAD: 0, TOS: 0, 0, 0}  PUSH -1 ('-1' @ 8:PUSH)
DEBUG:root:{TICK: 158, PC: 8, HEAD: 1, TOS: 0, 0, -1}  JNT 10 ('None' @ 9:WHILE)
DEBUG:root:{TICK: 159, PC: 9, HEAD: 0, TOS: 0, 0, 0}  JMP 7 ('None' @ 10:REPEAT)
DEBUG:root:{TICK: 160, PC: 7, HEAD: 0, TOS: 0, 0, 0}  PUSH -1 ('-1' @ 8:PUSH)
DEBUG:root:{TICK: 161, PC: 8, HEAD: 1, TOS: 0, 0, -1}  JNT 10 ('None' @ 9:WHILE)
DEBUG:root:{TICK: 162, PC: 9, HEAD: 0, TOS: 0, 0, 0}  JMP 7 ('None' @ 10:REPEAT)
DEBUG:root:{TICK: 163, PC: 7, HEAD: 0, TOS: 0, 0, 0}  PUSH -1 ('-1' @ 8:PUSH)
DEBUG:root:{TICK: 164, PC: 8, HEAD: 1, TOS: 0, 0, -1}  JNT 10 ('None' @ 9:WHILE)
DEBUG:root:{TICK: 165, PC: 9, HEAD: 0, TOS: 0, 0, 0}  JMP 7 ('None' @ 10:REPEAT)
DEBUG:root:{TICK: 166, PC: 7, HEAD: 0, TOS: 0, 0, 0}  PUSH -1 ('-1' @ 8:PUSH)
DEBUG:root:{TICK: 167, PC: 8, HEAD: 1, TOS: 0, 0, -1}  JNT 10 ('None' @ 9:WHILE)
DEBUG:root:{TICK: 168, PC: 9, HEAD: 0, TOS: 0, 0, 0}  JMP 7 ('None' @ 10:REPEAT)
DEBUG:root:{TICK: 169, PC: 7, HEAD: 0, TOS: 0, 0, 0}  PUSH -1 ('-1' @ 8:PUSH)
DEBUG:root:{TICK: 170, PC: 8, HEAD: 1, TOS: 0, 0, -1}  JNT 10 ('None' @ 9:WHILE)
DEBUG:root:{TICK: 171, PC: 9, HEAD: 0, TOS: 0, 0, 0}  JMP 7 ('None' @ 10:REPEAT)
DEBUG:root:{TICK: 172, PC: 7, HEAD: 0, TOS: 0, 0, 0}  PUSH -1 ('-1' @ 8:PUSH)
DEBUG:root:{TICK: 173, PC: 8, HEAD: 1, TOS: 0, 0, -1}  JNT 10 ('None' @ 9:WHILE)
DEBUG:root:{TICK: 174, PC: 9, HEAD: 0, TOS: 0, 0, 0}  JMP 7 ('None' @ 10:REPEAT)
DEBUG:root:{TICK: 175, PC: 7, HEAD: 0, TOS: 0, 0, 0}  PUSH -1 ('-1' @ 8:PUSH)
DEBUG:root:{TICK: 176, PC: 8, HEAD: 1, TOS: 0, 0, -1}  JNT 10 ('None' @ 9:WHILE)
DEBUG:root:{TICK: 177, PC: 9, HEAD: 0, TOS: 0, 0, 0}  JMP 7 ('None' @ 10:REPEAT)
DEBUG:root:{TICK: 178, PC: 7, HEAD: 0, TOS: 0, 0, 0}  PUSH -1 ('-1' @ 8:PUSH)
DEBUG:root:{TICK: 179, PC: 8, HEAD: 1, TOS: 0, 0, -1}  JNT 10 ('None' @ 9:WHILE)
DEBUG:root:{TICK: 180, PC: 9, HEAD: 0, TOS: 0, 0, 0}  JMP 7 ('None' @ 10:REPEAT)
DEBUG:root:{TICK: 181, PC: 7, HEAD: 0, TOS: 0, 0, 0}  PUSH -1 ('-1' @ 8:PUSH)
DEBUG:root:{TICK: 182, PC: 8, HEAD: 1, TOS: 0, 0, -1}  JNT 10 ('None' @ 9:WHILE)
DEBUG:root:{TICK: 183, PC: 9, HEAD: 0, TOS: 0, 0, 0}  JMP 7 ('None' @ 10:REPEAT)
DEBUG:root:{TICK: 184, PC: 7, HEAD: 0, TOS: 0, 0, 0}  PUSH -1 ('-1' @ 8:PUSH)
DEBUG:root:{TICK: 185, PC: 8, HEAD: 1, TOS: 0, 0, -1}  JNT 10 ('None' @ 9:WHILE)
DEBUG:root:{TICK: 186, PC: 9, HEAD: 0, TOS: 0, 0, 0}  JMP 7 ('None' @ 10:REPEAT)
DEBUG:root:{TICK: 187, PC: 7, HEAD: 0, TOS: 0, 0, 0}  PUSH -1 ('-1' @ 8:PUSH)
DEBUG:root:{TICK: 188, PC: 8, HEAD: 1, TOS: 0, 0, -1}  JNT 10 ('None' @ 9:WHILE)
DEBUG:root:{TICK: 189, PC: 9, HEAD: 0, TOS: 0, 0, 0}  JMP 7 ('None' @ 10:REPEAT)
DEBUG:root:{TICK: 190, PC: 7, HEAD: 0, TOS: 0, 0, 0}  PUSH -1 ('-1' @ 8:PUSH)
DEBUG:root:{TICK: 191, PC: 8, HEAD: 1, TOS: 0, 0, -1}  JNT 10 ('None' @ 9:WHILE)
DEBUG:root:{TICK: 192, PC: 9, HEAD: 0, TOS: 0, 0, 0}  JMP 7 ('None' @ 10:REPEAT)
DEBUG:root:{TICK: 193, PC: 7, HEAD: 0, TOS: 0, 0, 0}  PUSH -1 ('-1' @ 8:PUSH)
DEBUG:root:{TICK: 194, PC: 8, HEAD: 1, TOS: 0, 0, -1}  JNT 10 ('None' @ 9:WHILE)
DEBUG:root:{TICK: 195, PC: 9, HEAD: 0, TOS: 0, 0, 0}  JMP 7 ('None' @ 10:REPEAT)
DEBUG:root:{TICK: 196, PC: 7, HEAD: 0, TOS: 0, 0, 0}  PUSH -1 ('-1' @ 8:PUSH)
DEBUG:root:{TICK: 197, PC: 8, HEAD: 1, TOS: 0, 0, -1}  JNT 10 ('None' @ 9:WHILE)
DEBUG:root:{TICK: 198, PC: 9, HEAD: 0, TOS: 0, 0, 0}  JMP 7 ('None' @ 10:REPEAT)
DEBUG:root:{TICK: 199, PC: 7, HEAD: 0, TOS: 0, 0, 0}  PUSH -1 ('-1' @ 8:PUSH)
DEBUG:root:{TICK: 200, PC: 8, HEAD: 1, TOS: 0, 0, -1}  JNT 10 ('None' @ 9:WHILE)
DEBUG:root:{TICK: 201, PC: 9, HEAD: 0, TOS: 0, 0, 0}  JMP 7 ('None' @ 10:REPEAT)
DEBUG:root:{TICK: 202, PC: 7, HEAD: 0, TOS: 0, 0, 0}  PUSH -1 ('-1' @ 8:PUSH)
DEBUG:root:{TICK: 203, PC: 8, HEAD: 1, TOS: 0, 0, -1}  JNT 10 ('None' @ 9:WHILE)
DEBUG:root:{TICK: 204, PC: 9, HEAD: 0, TOS: 0, 0, 0}  JMP 7 ('None' @ 10:REPEAT)
DEBUG:root:{TICK: 205, PC: 7, HEAD: 0, TOS: 0, 0, 0}  PUSH -1 ('-1' @ 8:PUSH)
DEBUG:root:{TICK: 206, PC: 8, HEAD: 1, TOS: 0, 0, -1}  JNT 10 ('None' @ 9:WHILE)
DEBUG:root:{TICK: 207, PC: 9, HEAD: 0, TOS: 0, 0, 0}  JMP 7 ('None' @ 10:REPEAT)
DEBUG:root:{TICK: 208, PC: 7, HEAD: 0, TOS: 0, 0, 0}  PUSH -1 ('-1' @ 8:PUSH)
DEBUG:root:{TICK: 209, PC: 8, HEAD: 1, TOS: 0, 0, -1}  JNT 10 ('None' @ 9:WHILE)
DEBUG:root:{TICK: 210, PC: 9, HEAD: 0, TOS: 0, 0, 0}  JMP 7 ('None' @ 10:REPEAT)
DEBUG:root:{TICK: 211, PC: 7, HEAD: 0, TOS: 0, 0, 0}  PUSH -1 ('-1' @ 8:PUSH)
DEBUG:root:{TICK: 212, PC: 8, HEAD: 1, TOS: 0, 0, -1}  JNT 10 ('None' @ 9:WHILE)
DEBUG:root:{TICK: 213, PC: 9, HEAD: 0, TOS: 0, 0, 0}  JMP 7 ('None' @ 10:REPEAT)
DEBUG:root:{TICK: 214, PC: 7, HEAD: 0, TOS: 0, 0, 0}  PUSH -1 ('-1' @ 8:PUSH)
DEBUG:root:{TICK: 215, PC: 8, HEAD: 1, TOS: 0, 0, -1}  JNT 10 ('None' @ 9:WHILE)
DEBUG:root:{TICK: 216, PC: 9, HEAD: 0, TOS: 0, 0, 0}  JMP 7 ('None' @ 10:REPEAT)
DEBUG:root:{TICK: 217, PC: 7, HEAD: 0, TOS: 0, 0, 0}  PUSH -1 ('-1' @ 8:PUSH)
DEBUG:root:{TICK: 218, PC: 8, HEAD: 1, TOS: 0, 0, -1}  JNT 10 ('None' @ 9:WHILE)
DEBUG:root:{TICK: 219, PC: 9, HEAD: 0, TOS: 0, 0, 0}  JMP 7 ('None' @ 10:REPEAT)
DEBUG:root:{TICK: 220, PC: 7, HEAD: 0, TOS: 0, 0, 0}  PUSH -1 ('-1' @ 8:PUSH)
DEBUG:root:{TICK: 221, PC: 8, HEAD: 1, TOS: 0, 0, -1}  JNT 10 ('None' @ 9:WHILE)
DEBUG:root:{TICK: 222, PC: 9, HEAD: 0, TOS: 0, 0, 0}  JMP 7 ('None' @ 10:REPEAT)
DEBUG:root:{TICK: 223, PC: 7, HEAD: 0, TOS: 0, 0, 0}  PUSH -1 ('-1' @ 8:PUSH)
DEBUG:root:{TICK: 224, PC: 8, HEAD: 1, TOS: 0, 0, -1}  JNT 10 ('None' @ 9:WHILE)
DEBUG:root:{TICK: 225, PC: 9, HEAD: 0, TOS: 0, 0, 0}  JMP 7 ('None' @ 10:REPEAT)
DEBUG:root:{TICK: 226, PC: 7, HEAD: 0, TOS: 0, 0, 0}  PUSH -1 ('-1' @ 8:PUSH)
DEBUG:root:{TICK: 227, PC: 8, HEAD: 1, TOS: 0, 0, -1}  JNT 10 ('None' @ 9:WHILE)
DEBUG:root:{TICK: 228, PC: 9, HEAD: 0, TOS: 0, 0, 0}  JMP 7 ('None' @ 10:REPEAT)
DEBUG:root:{TICK: 229, PC: 7, HEAD: 0, TOS: 0, 0, 0}  PUSH -1 ('-1' @ 8:PUSH)
DEBUG:root:{TICK: 230, PC: 8, HEAD: 1, TOS: 0, 0, -1}  JNT 10 ('None' @ 9:WHILE)
DEBUG:root:{TICK: 231, PC: 9, HEAD: 0, TOS: 0, 0, 0}  JMP 7 ('None' @ 10:REPEAT)
DEBUG:root:{TICK: 232, PC: 7, HEAD: 0, TOS: 0, 0, 0}  PUSH -1 ('-1' @ 8:PUSH)
DEBUG:root:{TICK: 233, PC: 8, HEAD: 1, TOS: 0, 0, -1}  JNT 10 ('None' @ 9:WHILE)
DEBUG:root:{TICK: 234, PC: 9, HEAD: 0, TOS: 0, 0, 0}  JMP 7 ('None' @ 10:REPEAT)
DEBUG:root:{TICK: 235, PC: 7, HEAD: 0, TOS: 0, 0, 0}  PUSH -1 ('-1' @ 8:PUSH)
DEBUG:root:{TICK: 236, PC: 8, HEAD: 1, TOS: 0, 0, -1}  JNT 10 ('None' @ 9:WHILE)
DEBUG:root:{TICK: 237, PC: 9, HEAD: 0, TOS: 0, 0, 0}  JMP 7 ('None' @ 10:REPEAT)
DEBUG:root:{TICK: 238, PC: 7, HEAD: 0, TOS: 0, 0, 0}  PUSH -1 ('-1' @ 8:PUSH)
DEBUG:root:{TICK: 239, PC: 8, HEAD: 1, TOS: 0, 0, -1}  JNT 10 ('None' @ 9:WHILE)
DEBUG:root:{TICK: 240, PC: 9, HEAD: 0, TOS: 0, 0, 0}  JMP 7 ('None' @ 10:REPEAT)
DEBUG:root:{TICK: 241, PC: 7, HEAD: 0, TOS: 0, 0, 0}  PUSH -1 ('-1' @ 8:PUSH)
DEBUG:root:{TICK: 242, PC: 8, HEAD: 1, TOS: 0, 0, -1}  JNT 10 ('None' @ 9:WHILE)
DEBUG:root:{TICK: 243, PC: 9, HEAD: 0, TOS: 0, 0, 0}  JMP 7 ('None' @ 10:REPEAT)
DEBUG:root:{TICK: 244, PC: 7, HEAD: 0, TOS: 0, 0, 0}  PUSH -1 ('-1' @ 8:PUSH)
DEBUG:root:{TICK: 245, PC: 8, HEAD: 1, TOS: 0, 0, -1}  JNT 10 ('None' @ 9:WHILE)
DEBUG:root:{TICK: 246, PC: 9, HEAD: 0, TOS: 0, 0, 0}  JMP 7 ('None' @ 10:REPEAT)
DEBUG:root:{TICK: 247, PC: 7, HEAD: 0, TOS: 0, 0, 0}  PUSH -1 ('-1' @ 8:PUSH)
DEBUG:root:{TICK: 248, PC: 8, HEAD: 1, TOS: 0, 0, -1}  JNT 10 ('None' @ 9:WHILE)
DEBUG:root:{TICK: 249, PC: 9, HEAD: 0, TOS: 0, 0, 0}  JMP 7 ('None' @ 10:REPEAT)
DEBUG:root:{TICK: 250, PC: 7, HEAD: 0, TOS: 0, 0, 0}  PUSH -1 ('-1' @ 8:PUSH)
DEBUG:root:{TICK: 251, PC: 8, HEAD: 1, TOS: 0, 0, -1}  JNT 10 ('None' @ 9:WHILE)
DEBUG:root:{TICK: 252, PC: 9, HEAD: 0, TOS: 0, 0, 0}  JMP 7 ('None' @ 10:REPEAT)
DEBUG:root:{TICK: 253, PC: 7, HEAD: 0, TOS: 0, 0, 0}  PUSH -1 ('-1' @ 8:PUSH)
DEBUG:root:{TICK: 254, PC: 8, HEAD: 1, TOS: 0, 0, -1}  JNT 10 ('None' @ 9:WHILE)
DEBUG:root:{TICK: 255, PC: 9, HEAD: 0, TOS: 0, 0, 0}  JMP 7 ('None' @ 10:REPEAT)
DEBUG:root:{TICK: 256, PC: 7, HEAD: 0, TOS: 0, 0, 0}  PUSH -1 ('-1' @ 8:PUSH)
DEBUG:root:{TICK: 257, PC: 8, HEAD: 1, TOS: 0, 0, -1}  JNT 10 ('None' @ 9:WHILE)
DEBUG:root:{TICK: 258, PC: 9, HEAD: 0, TOS: 0, 0, 0}  JMP 7 ('None' @ 10:REPEAT)
DEBUG:root:{TICK: 259, PC: 7, HEAD: 0, TOS: 0, 0, 0}  PUSH -1 ('-1' @ 8:PUSH)
DEBUG:root:{TICK: 260, PC: 8, HEAD: 1, TOS: 0, 0, -1}  JNT 10 ('None' @ 9:WHILE)
DEBUG:root:{TICK: 261, PC: 9, HEAD: 0, TOS: 0, 0, 0}  JMP 7 ('None' @ 10:REPEAT)
DEBUG:root:{TICK: 262, PC: 7, HEAD: 0, TOS: 0, 0, 0}  PUSH -1 ('-1' @ 8:PUSH)
DEBUG:root:{TICK: 263, PC: 8, HEAD: 1, TOS: 0, 0, -1}  JNT 10 ('None' @ 9:WHILE)
DEBUG:root:{TICK: 264, PC: 9, HEAD: 0, TOS: 0, 0, 0}  JMP 7 ('None' @ 10:REPEAT)
DEBUG:root:{TICK: 265, PC: 7, HEAD: 0, TOS: 0, 0, 0}  PUSH -1 ('-1' @ 8:PUSH)
DEBUG:root:{TICK: 266, PC: 8, HEAD: 1, TOS: 0, 0, -1}  JNT 10 ('None' @ 9:WHILE)
DEBUG:root:{TICK: 267, PC: 9, HEAD: 0, TOS: 0, 0, 0}  JMP 7 ('None' @ 10:REPEAT)
DEBUG:root:{TICK: 268, PC: 7, HEAD: 0, TOS: 0, 0, 0}  PUSH -1 ('-1' @ 8:PUSH)
DEBUG:root:{TICK: 269, PC: 8, HEAD: 1, TOS: 0, 0, -1}  JNT 10 ('None' @ 9:WHILE)
DEBUG:root:{TICK: 270, PC: 9, HEAD: 0, TOS: 0, 0, 0}  JMP 7 ('None' @ 10:REPEAT)
DEBUG:root:{TICK: 271, PC: 7, HEAD: 0, TOS: 0, 0, 0}  PUSH -1 ('-1' @ 8:PUSH)
DEBUG:root:{TICK: 272, PC: 8, HEAD: 1, TOS: 0, 0, -1}  JNT 10 ('None' @ 9:WHILE)
DEBUG:root:{TICK: 273, PC: 9, HEAD: 0, TOS: 0, 0, 0}  JMP 7 ('None' @ 10:REPEAT)
DEBUG:root:{TICK: 274, PC: 7, HEAD: 0, TOS: 0, 0, 0}  PUSH -1 ('-1' @ 8:PUSH)
DEBUG:root:{TICK: 275, PC: 8, HEAD: 1, TOS: 0, 0, -1}  JNT 10 ('None' @ 9:WHILE)
DEBUG:root:{TICK: 276, PC: 9, HEAD: 0, TOS: 0, 0, 0}  JMP 7 ('None' @ 10:REPEAT)
DEBUG:root:{TICK: 277, PC: 7, HEAD: 0, TOS: 0, 0, 0}  PUSH -1 ('-1' @ 8:PUSH)
DEBUG:root:{TICK: 278, PC: 8, HEAD: 1, TOS: 0, 0, -1}  JNT 10 ('None' @ 9:WHILE)
DEBUG:root:{TICK: 279, PC: 9, HEAD: 0, TOS: 0, 0, 0}  JMP 7 ('None' @ 10:REPEAT)
DEBUG:root:{TICK: 280, PC: 7, HEAD: 0, TOS: 0, 0, 0}  PUSH -1 ('-1' @ 8:PUSH)
DEBUG:root:{TICK: 281, PC: 8, HEAD: 1, TOS: 0, 0, -1}  JNT 10 ('None' @ 9:WHILE)
DEBUG:root:{TICK: 282, PC: 9, HEAD: 0, TOS: 0, 0, 0}  JMP 7 ('None' @ 10:REPEAT)
DEBUG:root:{TICK: 283, PC: 7, HEAD: 0, TOS: 0, 0, 0}  PUSH -1 ('-1' @ 8:PUSH)
DEBUG:root:{TICK: 284, PC: 8, HEAD: 1, TOS: 0, 0, -1}  JNT 10 ('None' @ 9:WHILE)
DEBUG:root:{TICK: 285, PC: 9, HEAD: 0, TOS: 0, 0, 0}  JMP 7 ('None' @ 10:REPEAT)
DEBUG:root:{TICK: 286, PC: 7, HEAD: 0, TOS: 0, 0, 0}  PUSH -1 ('-1' @ 8:PUSH)
DEBUG:root:{TICK: 287, PC: 8, HEAD: 1, TOS: 0, 0, -1}  JNT 10 ('None' @ 9:WHILE)
DEBUG:root:{TICK: 288, PC: 9, HEAD: 0, TOS: 0, 0, 0}  JMP 7 ('None' @ 10:REPEAT)
DEBUG:root:{TICK: 289, PC: 7, HEAD: 0, TOS: 0, 0, 0}  PUSH -1 ('-1' @ 8:PUSH)
DEBUG:root:{TICK: 290, PC: 8, HEAD: 1, TOS: 0, 0, -1}  JNT 10 ('None' @ 9:WHILE)
DEBUG:root:{TICK: 291, PC: 9, HEAD: 0, TOS: 0, 0, 0}  JMP 7 ('None' @ 10:REPEAT)
DEBUG:root:{TICK: 292, PC: 7, HEAD: 0, TOS: 0, 0, 0}  PUSH -1 ('-1' @ 8:PUSH)
DEBUG:root:{TICK: 293, PC: 8, HEAD: 1, TOS: 0, 0, -1}  JNT 10 ('None' @ 9:WHILE)
DEBUG:root:{TICK: 294, PC: 9, HEAD: 0, TOS: 0, 0, 0}  JMP 7 ('None' @ 10:REPEAT)
DEBUG:root:{TICK: 295, PC: 7, HEAD: 0, TOS: 0, 0, 0}  PUSH -1 ('-1' @ 8:PUSH)
DEBUG:root:{TICK: 296, PC: 8, HEAD: 1, TOS: 0, 0, -1}  JNT 10 ('None' @ 9:WHILE)
DEBUG:root:{TICK: 297, PC: 9, HEAD: 0, TOS: 0, 0, 0}  JMP 7 ('None' @ 10:REPEAT)
DEBUG:root:{TICK: 298, PC: 7, HEAD: 0, TOS: 0, 0, 0}  PUSH -1 ('-1' @ 8:PUSH)
DEBUG:root:{TICK: 299, PC: 8, HEAD: 1, TOS: 0, 0, -1}  JNT 10 ('None' @ 9:WHILE)
DEBUG:root:{TICK: 300, PC: 9, HEAD: 0, TOS: 0, 0, 0}  JMP 7 ('None' @ 10:REPEAT)
DEBUG:root:{TICK: 301, PC: 7, HEAD: 0, TOS: 0, 0, 0}  PUSH -1 ('-1' @ 8:PUSH)
DEBUG:root:{TICK: 302, PC: 8, HEAD: 1, TOS: 0, 0, -1}  JNT 10 ('None' @ 9:WHILE)
DEBUG:root:{TICK: 303, PC: 9, HEAD: 0, TOS: 0, 0, 0}  JMP 7 ('None' @ 10:REPEAT)
DEBUG:root:{TICK: 304, PC: 7, HEAD: 0, TOS: 0, 0, 0}  PUSH -1 ('-1' @ 8:PUSH)
DEBUG:root:{TICK: 305, PC: 8, HEAD: 1, TOS: 0, 0, -1}  JNT 10 ('None' @ 9:WHILE)
DEBUG:root:{TICK: 306, PC: 9, HEAD: 0, TOS: 0, 0, 0}  JMP 7 ('None' @ 10:REPEAT)
DEBUG:root:{TICK: 307, PC: 7, HEAD: 0, TOS: 0, 0, 0}  PUSH -1 ('-1' @ 8:PUSH)
DEBUG:root:{TICK: 308, PC: 8, HEAD: 1, TOS: 0, 0, -1}  JNT 10 ('None' @ 9:WHILE)
DEBUG:root:{TICK: 309, PC: 9, HEAD: 0, TOS: 0, 0, 0}  JMP 7 ('None' @ 10:REPEAT)
ERROR:root:Too long execution!
output: hello
instr_counter:  300 ticks: 310
```

| ФИО             | алг.  | code байт | code инстр. | инстр. | такт.  | вариант                                                |
|-----------------|-------|-----------|-------------|--------|--------|--------------------------------------------------------|
| Кошкарбаев Н.Ю. | hello | -         | 14          | 118    | 222    | forth, stack, neum, hw, instr, struct, trap, mem, prob2|
| Кошкарбаев Н.Ю. | cat   | -         | 9           | limit  | 309    | forth, stack, neum, hw, instr, struct, trap, mem, prob2|
| Кошкарбаев Н.Ю. | prob2 | -         | 40          | 654    | 822    | forth, stack, neum, hw, instr, struct, trap, mem, prob2|
